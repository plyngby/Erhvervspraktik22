# This python file uses the following encoding: utf-8
from __future__ import print_function
from ase.visualize import view
from ase.optimize import QuasiNewton
from gpaw import GPAW
from ase.io import Trajectory
import matplotlib.pyplot as plt
from helpers import find_O2_line, find_central_angle
from numpy.random import normal
from ase.structure import molecule

system = molecule('H2O2')
magnitude = 0.1
for atom in system:
    noise = normal(0, magnitude, (3,))
    atom.position += noise
# system = read('h2o2-start.traj')
system.center(vacuum=5)
view(system)

calc = GPAW(h=0.22,
            xc='PBE',
            mode='lcao',
            basis='dzp')

system.set_calculator(calc)

opt = QuasiNewton(system, trajectory='h2o2.traj')

opt.run(fmax=0.05)

atoms = Trajectory('h2o2.traj')
e_n = []
for config in atoms:
    e_n += [config.get_potential_energy()]

O2_line = find_O2_line(atoms[-1])
vector_1 = None
for atom in atoms[-1]:
    if atom.symbol == 'H':
        if vector_1 is None:
            vector_1 = O2_line.find_vector_from_point(atom.position)
        else:
            vector_2 = O2_line.find_vector_from_point(atom.position)
angle = find_central_angle(vector_1, vector_2)
print("Vinklen mellem de to H atomer "
      "i forhold til søjlen af O atomer er {0}".format(angle))
with open('H2O2.txt', 'w') as fd:
    print("Energi for brintoverilte: {0}.".format(e_n[-1]), file=fd)
view(system)

plt.plot(e_n)
plt.ylabel('Energi')
plt.title('Energi for H2O2 som funktion af tid')
plt.show()
