import numpy as np


class Line():
    def __init__(self, direction, offset):
        self.direction = direction
        self.offset = offset

    def find_vector_from_point(self, point):
        delta = point - self.offset
        return delta - self.direction*np.dot(self.direction, delta)


def norm(x):
    return np.sqrt(np.sum(x*x))


def find_central_angle(vector_1, vector_2):
    a = norm(vector_1)
    b = norm(vector_2)
    c = norm(vector_1 - vector_2)
    cos_theta = (a**2 + b**2 - c**2)/(2*a*b)
    return 180/np.pi * np.arccos(cos_theta)


def find_O2_line(atoms):
    direction = 0
    for atom in atoms:
        if atom.symbol == 'O':
            offset = atom.position
            direction += atom.position
            direction *= -1
    return Line(direction, offset)
