import numpy as np
from ase.lattice.surface import fcc111
from ase.visualize import view
from ase.build import molecule, add_adsorbate
from ase.parallel import paropen
from gpaw import GPAW
import sys

element='Al'
gitterkonstant=4.046

# 3.62, 3.912, 4.065, 3.499

overflade=fcc111(element,size=(2,2,3),a=gitterkonstant)

OH = molecule('OH')
OH.rotate('x',2*np.pi/3.)

hoejde=1.9
add_adsorbate(overflade, OH, hoejde, 'ontop')

OH.rotate('x',-2*2*np.pi/3.)
add_adsorbate(overflade, OH, hoejde, 'ontop', offset=1.)

overflade.center(vacuum=3.,axis=2)

calc = GPAW(h=0.22,xc='PBE',mode='lcao',
            basis='dzp',kpts=(4,4,1),spinpol=False,
            txt='data/{0}_overflade_med_2OH.txt'.format(element))

overflade.set_calculator(calc)

overflade.set_initial_magnetic_moments([0]*len(overflade))

if len(sys.argv)>1:
    if sys.argv[1]=='view':
        view(overflade)
else:
    entropi=.94
    energi=overflade.get_potential_energy()
    energi+=entropi
    with paropen('{0}_overflade_med_2OH_energi.txt'.format(element),'w') as f:
	      f.write('{0}'.format(energi))
