import numpy as np
from ase.build import molecule
from gpaw import GPAW
from ase.optimize import BFGS
from ase.parallel import paropen

formel='H2O2'

molekyle = molecule(formel)

molekyle.center(vacuum=4)

calc = GPAW(h=0.22,xc='PBE',mode='lcao',
            basis='dzp',txt='data/{0}.txt'.format(formel))

molekyle.set_calculator(calc)

pos=molekyle.get_positions()
np.random.seed(0)
molekyle.set_positions(pos+.2*np.random.randn(pos.shape[0],pos.shape[1]))

dyn = BFGS(molekyle)

dyn.run(fmax=0.05)

with paropen('{0}_energi.txt'.format(formel),'w') as f:
    f.write('{0}'.format(molekyle.get_potential_energy()))

