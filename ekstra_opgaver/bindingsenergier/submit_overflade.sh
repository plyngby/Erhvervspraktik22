#!/bin/sh
#PBS -N overflade
#PBS -e overflade.err
#PBS -o overflade.log
module load asegpaw
mpiexec -np 4 gpaw-python overflade.py
