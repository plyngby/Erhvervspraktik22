#!/bin/sh
#PBS -N overflade
#PBS -e overflade.err
#PBS -o overflade.log
module load asegpaw
mpiexec -np 8 gpaw-python overflade_med_2OH.py
