import numpy as np
from ase.lattice.surface import fcc111
from ase.visualize import view
from gpaw import GPAW
from ase.parallel import paropen
import sys

element='Al'
gitterkonstant=4.046

overflade=fcc111(element,size=(2,2,3),a=gitterkonstant)

overflade.center(vacuum=3,axis=2)

calc = GPAW(h=0.22,xc='PBE',mode='lcao',
            basis='dzp',kpts=(4,4,1),
            txt='data/{0}_overflade.txt'.format(element))

overflade.set_calculator(calc)

if len(sys.argv)>1:
    if sys.argv[1]=='view':
        view(overflade)
else:
    with paropen('{0}_overflade_energi.txt'.format(element),'w') as f:
	      f.write('{0}'.format(overflade.get_potential_energy()))
