import numpy as np
from ase.units import _hplanck, _c, _e
from gpaw.response.df import DielectricFunction


lambda_min = 100
lambda_max = 1200
constant = _hplanck * _c / _e * 1e9
E_max = constant/lambda_min
E_min = constant/lambda_max

filename = 'NaCl_df'

freqs = np.linspace(E_min, E_max, 201)
q = np.array([0., 1e-5, 0.])

df = DielectricFunction(calc='NaCl_gs.gpw')
#        txt=filename + '.out')
df.get_dielectric_function(filename='NaCl_df.dat')


