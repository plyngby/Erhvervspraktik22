import numpy as np
from ase.units import _hplanck, _c, _e
from gpaw.response.df import DielectricFunction


name = 'Cu'
lambda_min = 100
lambda_max = 1200
constant = _hplanck * _c / _e * 1e9
E_max = constant/lambda_min
E_min = constant/lambda_max
freqs = np.linspace(E_min, E_max, 201)

# Part 2 : Spectrum calculation          # DF: dielectric function object
df = DielectricFunction(calc= name + '.gpw',   # Ground state gpw file (with wavefunction) as input
			frequencies = freqs,                        
			domega0=0.05,
			hilbert = False,
			ecut = 200)    # Using nonlinear frequency grid
df.get_dielectric_function(filename = name + '_df.dat')             # By default, a file called 'df.csv' is generated
