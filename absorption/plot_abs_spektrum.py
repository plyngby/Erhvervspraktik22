#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from ase.units import _c, _hplanck, _e

# Her angiver du navnet på materialet
compound = '...'


# Nedenstående kode tegner en graf af absorptionsspektret
data = np.loadtxt(compound + '_df.dat', delimiter=",")

freqs = data[:,0]
freqs[0] = 1e-6
constant = _c * _hplanck / _e * 1e9
plt.xlim([100, 1000])
plt.plot(constant/data[:,0], data[:,-1], '-k')
plt.xlabel(u'Bølgelængde (nm)')
plt.title(u'Absorptionsspektrum for ' + compound)
plt.show()
