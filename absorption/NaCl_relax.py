import numpy as np
import pickle
from ase.lattice.compounds import NaCl
from ase.io.trajectory import Trajectory
from ase.visualize import view
from gpaw import GPAW, FermiDirac
import matplotlib.pyplot as plt
from ase.parallel import rank

a0 = 5.64

filename = 'NaCl_relax'

calc = GPAW(xc='PBE',
            kpts=(4, 4, 4),
            occupations=FermiDirac(0.01))

traj = Trajectory(filename=filename + '.traj', mode='w')

a_n = np.linspace(0.9*a0, 1.1*a0, 11)
e_n = np.zeros(len(a_n))
for n in range(len(a_n)):
    atoms = NaCl(['Na', 'Cl'], latticeconstant=a_n[n])
    atoms.set_calculator(calc)
    e_n[n] = atoms.get_potential_energy()
    traj.write(atoms=atoms)

data = np.array([a_n, e_n])
np.savetxt(filename + '.dat', data)
if rank == 0:
	plt.plot(a_n, e_n)
	plt.title('Energi for NaCl som funktion af gitterkonstanten')
	plt.xlabel('Gitterkonstant')
	plt.ylabel('Energi')
	plt.show()
view(atoms)
