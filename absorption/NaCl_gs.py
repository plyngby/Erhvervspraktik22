import numpy as np
import pickle
from ase.lattice.compounds import NaCl
from ase.io.trajectory import Trajectory
from ase.visualize import view
from gpaw import GPAW, FermiDirac, PW
a = 5.64

filename = 'NaCl_gs'

atoms = NaCl(['Na', 'Cl'], latticeconstant=a)

calc = GPAW(mode=PW(900),
            xc='PBE',
            kpts=(4, 4, 4),
            basis='dzp',
            nbands=80,
            convergence={'bands': 70},
            eigensolver='cg',
            occupations=FermiDirac(0.01))

atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write(filename + '.gpw', 'all')
