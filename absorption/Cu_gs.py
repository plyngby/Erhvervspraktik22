import numpy as np
from ase.lattice import bulk
from gpaw import GPAW, PW, FermiDirac
from gpaw.response.df import DielectricFunction
from ase.parallel import rank
from ase.units import _hplanck, _c, _e

# Part 1: Ground state calculation
name = 'Cu'

atoms = bulk(name, 'fcc', a=3.610)   
calc = GPAW(mode = PW(900), 
	    kpts={'density': 5.0, 'gamma': False},
	    occupations=FermiDirac(0.01))   
 
atoms.set_calculator(calc)               
atoms.get_potential_energy()             
calc.diagonalize_full_hamiltonian(nbands=70)
calc.write(name + '.gpw', 'all')


