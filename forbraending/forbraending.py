from ase import *
from ase.visualize import *
from ase.io import *
from ase.optimize import *
#from gpaw import *
from ase.structure import molecule 
from ase.calculators.emt import EMT

molekyle = 'CH3OH'

mol = molecule(molekyle)

mol.center(vacuum=5)

#calc = GPAW(mode='lcao', basis='dzp', xc='PBE')
calc = EMT()

mol.set_calculator(calc)

E = mol.get_potential_energy()

fil = open(molekyle+'.txt', 'w')

fil.write(str(E))

fil.close()

view(mol)

